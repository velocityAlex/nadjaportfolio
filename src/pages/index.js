import React from 'react';
import Layout from "../components/layout"
import Card from "../components/card"
import Spacer from "../components/spacer"
import '../styles/index.scss';

function Index() {
  return (
    <div>
        <Layout />
        <main>
            <div>
                <h2>Hi, I’m Nadja. Let me introduce myself.</h2>
                <p class="p-intro">I’m a content writer. I like to break down ideas into digestible chunks of information and put a smile on the reader’s face in the process.</p>
                <p class="p-intro">I write a lot of blog posts, articles, essays and copy that either 1) makes you think, 2) helps you out, 3) makes you want to consume a product or service.</p>
                <p class="p-intro">Here are a few samples of my work:</p>
            </div>
            <Spacer />
            <Card articleHeader="A BEGINNER’S GUIDE TO SEO" articleExcerpt="Reading online articles about SEO, I noticed how much unnecessary jargon was used to explain the basics, despite the fact that they were meant for beginners. So I wrote a guide of my own, making SEO as easy to understand as possible." link="/guide-to-seo/" />
            <Card articleHeader="EASY" articleExcerpt="The easy snack bar is a little project I set for myself to show how I would sell a health product with as few words as possible." link="/easy-health-bar/" />
            <Card articleHeader="WHY CONTENT WRITING MATTERS" articleExcerpt="This short piece explains how important content writing is for your brand and why." link="/content-writing/" />
            <Card articleHeader="STOP ANXIETY FROM RUINING YOUR LIFE" articleExcerpt="Written for my mental health blog, Viktor’s Got A Point, the purpose of this article is to speak to people suffering from anxiety disorders and help them in their journey to recovery." link="/anxiety/" />
            <Card articleHeader="IF YOU CUT YOUR HAIR, YOU’LL LOOK LIKE A BOY" articleExcerpt="This essay deals with the importance of hair length for girls and how it affects women’s lives." link="/short-hair/" />
        </main>
    </div>
  );
}

export default Index;
