import React from 'react';
import Layout from "../components/layout"


function guide() {
  return (
    <div>
        <Layout />
        <main>
            <article>
                <h1>A BEGINNER’S GUIDE TO UNDERSTANDING SEO</h1>
                
                <p>In this article, I’ll explain what SEO is, why it’s important, and how it works in the most straightforward, concise way possible.</p>
                <p>Let’s begin.</p>

                <h2>1. What is SEO?</h2>

                <p>SEO stands for Search Engine Optimization.</p>
                <p>Search Engine Optimization means a website is optimized so that it can be as visible as possible on the Search Engine Results Pages (SERP).</p>
                <p>The Search Engine Results Pages (SERP) feature various links, or results, according to the query a user types into a search engine.</p>
                <p>The SERP tend to provide hundreds of results which are scattered throughout thousands of pages, depending on the topic.</p>
                <p>However, <a href="https://junto.digital/blog/seo-stats/">​75%* of users only stay on the first page.</a> </p>
                <p>That means that the purpose of SEO is to get a link to your website as close to the top of the first page of the SERPs as possible.</p>
                <p>The higher your website is placed, the more visible your website will be.</p>

                <h2>2. Why is SEO important?</h2>

                <p>Without SEO, your website will be extremely hard, if not impossible, to find without paying for advertising.</p>
                <p>That means no one will buy or consume your product.</p>
                <p>As an example, let’s take Marc here.</p>
                
                <p>Marc cultivates oranges and is ready to sell them.</p>
                <p>First, he drives to the marketplace where he knows a lot of people will be shopping.</p>
                <p>He then finds a free space to set up his fruit stand and puts up a sign that says: “I sell oranges!”.</p>
                <p>Passers-by see his sign, understand he sells oranges and become Marc’s first customers.</p>
                <p>If we translate that to an online business, the oranges are the product you are selling, the marketplace is the internet, the fruit stand and the sign that says “I sell oranges!” is your website, and the location of your fruit stand relies on SEO.</p>
                <p>In other words, SEO makes you visible to potential customers.</p>

                <h2>3. How does SEO work?</h2>

                <p>The purpose of SEO is to give the search engine what it wants: the most relevant answer possible (the results) to a user’s question (a query).</p>
                <p>To do that, it uses these three steps:</p>
                
                <ul>
                    <li>Crawling = scanning webpages</li>
                    <li>Indexing = storing webpages</li>
                    <li>Ranking = positioning webpages on the SERP</li>
                </ul>

                <p>Bots, which are also called “spiders”, crawl webpages, gather the newfound information in an index which the search engine then ranks based on:</p>

                <ul>
                    <li>the website’s content</li>
                    <li>the website’s usability</li>
                    <li>the website’s reputation among users and other websites</li>
                </ul>

                <h2>Conclusion</h2>

                <p>SEO is vital to online marketing, as it is the most efficient way to make your website visible on the SERPs and get traffic.</p>

                <p>It’s worth noting that SEO takes time to bear its fruit and is an ever-changing scene.</p>
                <p>For example, Google’s ranking algorithm (the basis on which Google ranks webpages) is updated multiple times a year, market interest fluctuates, keywords (the words people use in their queries) become less relevant, and content needs to be added and updated regularly.</p>
                <p>This means SEO requires constant tweaking and re-adjusting to be successful.</p>
                <p>But if you do it well, there is no limit to the number of potential visitors you can attract to your website.</p>
            </article>
        </main>
    </div>
  );
}

export default guide;
