import React from 'react';
import Layout from "../components/layout"


function short() {
  return (
    <div>
        <Layout />
        <main>
            <article>
                <h1>If you cut your hair, you’ll look like a boy</h1>
                
                <p>
                    “I’m cutting my​ hair ​short,” my best friend and my mother look at my
                    hair. They seem to have already said goodbye to my long locks and prepared
                    themselves to enter the first stage of grief: denial.
                </p>
                <p>
                    “I think you will regret it”.
                </p>
                <p>
                    I pass it over with a smile and avoid eye contact for another minute or
                    two.
                </p>
                <p>
                    Reiterating their words in my mind, I ask myself: do they mean to say that
                    my long hair is the only attribute that grants me the luxury of putting
                    forth my best assets?
                </p>
                <p>
                    Leaving my bloodthirsty ego aside, however, I know they each have their
                    reasons.
                </p>
                <p>
                    My mother’s, for instance, is a simple case of being unable to have long
                    hair because of their frailness​, a tangible blow growing-up.
                </p>
                <p>
                    And yet here she sits across from us, her full crimson lips curling into a
                    radiant smile while she sips her wine, her deer shaped eyes flickering with
                    the reflection of the fireplace.
                </p>
                <p>
                    My mother does not possess the lustrous head of hair a James Bond girl
                    would flaunt by piling it on top of her head above swinging diamond
                    earrings or pulling it back into a simple ponytail. And she certainly isn’t
                    tall enough to ​be​ a James Bond girl either. But her beauty ​isn’t​ any
                    less truthful to the eye.
                </p>
                <p>
                    As it happens, her primitive years made her feel otherwise.
                </p>
                <p>
                    It’s around nineteen eighty-five. My mother, staggering into the early
                    stages of puberty, sits in her uncle’s chair ​(who happens to be a
                    hairdresser)​ in anticipation of my grandmother’s imposed makeover. A few
                    hours later, my grandmother gives a nod of approval to her brother-in-law,
                    praising his work, while my mother stumbles home and cries in front of the
                    bedroom mirror, shoulder-length curls on each side of her head like a
                    poodle.
                </p>
                <p>
                    The eighties perm was not flattering.
                </p>
                <p>
                    She may have been the last child and the favourite (though a subtle
                    exercise for the family in that, they weren’t too strong on expressing
                    their feelings) but it wasn’t enough to make her feel like anything other
                    than the ugly little duckling of the tribe, and her older, more fashionable
                    sister made sure of it.
                </p>
                <p>
                    As a result of unsuccessful makeovers, rude french hairdressers and a
                    bashful, limpid set of hair too thin to cover her left detached earlobe, my
                    mother was under the impression that she had been prohibited from entering
                    the folds of “womanhood” and was forever girdled in the tale of the ugly
                    duckling.
                </p>
                <p>
                    Just as blossoming breasts, a low waist-to-hip ratio and a shot at the
                    ceiling symbolise your graduation into “womanhood”, long hair ensures your
                    admission into the “girly club".
                </p>
                <p>
                    Keep in mind that I experienced childhood in the early 2000s and wasn’t
                    admitted into the “girly club” myself, so it’s possible that I still foster
                    some resentment.
                </p>
                <p>
                    All the popular girls in my school did not have long hair. But rest
                    assured, the pretty ones did.
                </p>
                <p>
                    One study led by Gizella Baktay-Korsos in five Hungarian primary schools,
                    published in 1999, in the review of psychology, found that girls with long
                    hair were more popular than their short-haired counterparts.
                </p>
                <p>
                    The study suggests that long-haired girls, regardless of their age, might
                    be better cared-for by their parents since long hair requires more time to
                    maintain and good personal hygiene. This, in turn, affords them a better
                    status within their groups and communities.
                </p>
                <p>
                    In their shoes, would I have been a more confident, more outgoing child,
                    praised for being the offspring of a well-mannered and well-integrated
                    family?
                </p>
                <p>
                    It would be indulgent to pretend that the people you admire growing up have
                    it all poured out for them in the dip of their palm without so much as a
                    wave of their finger.
                </p>
                <p>
                    Neither of us knows what goes on behind closed doors and the ones we
                    consider to be perfect are so flawed we might be taking our own luck for
                    granted.
                </p>
                <p>
                    My mother, for instance, kept my hair short out of fear that I should grow
                    up to inherit her feeble roots (she must have heard somewhere that keeping
                    your baby’s hair short will strengthen them). But once she allowed me to
                    let them grow I was unwilling to put time and effort into maintaining them.
                </p>
                <p>
                    My hair, though only a little below the shoulders, were clean (thankfully)
                    but frizzy and temperamental. Hardly a blueprint for the little girl’s
                    dream.
                </p>
                <p>
                    Back to two thousand and six, I’m sitting ​on the edge of my bed, arms
                    crossed, head down and determined to proclaim my free-will as a
                    nine-year-old. I stubbornly try every tactic I can think of (though I
                    ​possessed​ little wit in that department) to avoid yanking a comb through
                    what I considered to be uncombable hair (so why bother?) when I could be
                    playing Sims 2 instead.
                </p>
                <p>
                    Little did I know that pain for the sake of hygiene and conformity is
                    something most girls learn to incorporate into their daily lives (at the
                    risk of joining another, yet unknown breed of female outcasts). Eventually,
                    I gave in to waxing my legs and my moustache and I gave in to occasionally
                    plucking my eyebrows even though it makes my eyes water. Likewise, I
                    learned to take care of my hair.
                </p>
                <p>
                    This brings me to my present position, eying the idle pair of scissors by
                    the sink and reflecting upon six years of self-deprivation for the sake of
                    conformity. The Beauty Myth, as conferred by Naomi Wolf, is for many women
                    an unbreakable chain of misery where each of our milestones is shrouded
                    with the unshakable guilt of not being enough.
                </p>
                <p>
                    Let’s face it, no matter how much we rebel against the misogynistic axioms
                    that infiltrate our lives and our beings, by doing so, we admit to having
                    succumbed to ideals which we would not support were it not for the depth at
                    which history is infused in every person’s heritage.
                </p>
                <p>
                    And if I look into my eyes​ ​long enough, ​I start to realise that no
                    matter how much I try to incarnate my mother’s conception of beauty, my
                    inner life remains as hollow as the ends it can’t meet.
                </p>

            </article>
        </main>
    </div>
  );
}

export default short;
