import React from 'react';
import Layout from "../components/layout"


function content() {
  return (
    <div>
        <Layout />
        <main>
            <article>
                <h1>Why content writing matters</h1>
                <p>
                    An eye-catching logo is great for your brand.
                </p>
                <p>
                    So is the awesome website your customers navigate through to find the best
                    product they’ve come across in years (like the best quality notebooks a
                    writer like me could want).
                </p>
                <p>
                    But there’s one problem. Your content sucks.
                </p>
                <p>
                    The problem isn’t what you’ve written or even how you’ve written it
                    (although it’s connected to that), the problem is your content doesn’t
                    represent your brand.
                </p>
                <p>
                    It’s too generic, too formal, and even a little patronizing.
                </p>
                <p>
                    You mix up your pronouns so that no one even knows who’s written this
                    description and for what purpose, other than to tell me this one’s dark
                    blue.
                </p>
                <p>
                    In short, it doesn’t speak to your audience.
                </p>
                <p>
                    Where’s the connection? Where’s the story? Where’s the busy office creating
                    all those beautiful notebooks behind the scenes?
                </p>
                <p>
                    To stand out from the crowd of the internet where big companies take the
                    lead, you need to focus on what you’ve got to offer that they don’t.
                </p>
                <p>
                    That something is your voice.
                </p>
                <p>
                    An audience doesn’t want to pour their money into some virtual robotic cash
                    machine with the knowledge that they’ll receive a notebook in the near
                    future.
                </p>
                <p>
                    An audience wants to experience your product.
                </p>
                <p>
                    Move them with your words and captivate their imagination in a language
                    they understand. Then two days later, when a delightful notebook is being
                    delivered at their door, they’ll feel like rubbing their faces on the nice
                    fabric or sniffing the paper, making your product a sentimental item they
                    won’t want to get anywhere else.
                </p>
                <p>
                    This is why content writing matters.
                </p>
                <p>
                    It engages your audience, boosts your ranking on the SERP and creates new
                    leads because people know who you are and what you stand for.
                </p>
            </article>
        </main>
    </div>
  );
}

export default content;
