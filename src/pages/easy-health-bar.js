import React from "react"
import Layout from "../components/layout"
import { StaticImage } from "gatsby-plugin-image"




function easy() {
    return (
      <div>
          <Layout />
          <main className="easyMain">
            <div className="easy">
                <div className="easyHeader">
                    <div class="inner">
                        <StaticImage
                            src="../images/peach.png"
                            alt="A peach"
                            placeholder="blurred"
                            layout="fixed"
                            width={100}
                            height={100}
                        />
                        <p>Healthy snacks</p>
                        <p>don’t have to be boring. </p>
                        <p>We want</p>
                        <p>snacks​ ​to be</p>
                        <p>simple</p>
                        <p>tasty</p>
                        <p>100% carefree.</p>
                    </div>
                        
                </div>
                <div className="easyText">
                    <div>
                        <p><span>easy​</span> started with one</p>
                        <p>simple idea: to make</p> 
                        <p>healthy snacks less </p>
                        <p>boring and more </p>
                        <p>delicious.</p>
                    </div>
                    <div>
                        <p>Hundreds of recipes</p>
                        <p>later, we found the </p>
                        <p>perfect balance </p>
                        <p>between a </p>
                        <p>mouth-wateringly good, </p>
                        <p>100% organic</p>
                        <p>and 100% carefree treat,</p>
                        <p>making healthy choices</p>
                        <p>easy choices.</p>
                    </div>
                    <div>
                        <p className="p-larger">Don’t wait any longer.</p>
                        <p className="p-larger">Grab one!</p>
                    </div>
                </div>
            </div>
          </main>
      </div>
    );
  }
  
  export default easy;