import React from 'react';
import Layout from "../components/layout"


function anxiety() {
  return (
    <div>
        <Layout />
        <main>
            <article>
                <h1>Stop your anxiety from ruining your life</h1>
                                
                <p>
                    “Men are disturbed not by things, but by the views which they take on them”
                    Epictetus, the enchiridion
                </p>
                <p>
                    If you are a prisoner of your own thoughts, you should know that you’re
                    also the warden who holds the keys to your cell.
                </p>
                <p>
                    However, the constant worry that defines anxiety disorders has a strange
                    way of telling you otherwise. It’s like being in a controlling relationship
                    where your lover convinces you that you would be lost without him and
                    unable to cope.
                </p>
                <p>
                    But by staying, you take away any possibility of learning how to regulate
                    your emotions and face what scares you most: freedom.
                </p>
                <p>
                    But what does freedom even look like?
                </p>
                <ol start="1">
                    <li>
                        When you want to want to be free
                    </li>
                    <li>
                        When freedom means responsibility
                    </li>
                    <li>
                        When you decide to move on
                    </li>
                </ol>

                <h2>When you want to want to be free</h2>

                <p>
                    Tell me, when was the last time you pictured a life free of anxiety? Have
                    you ever considered it?
                </p>
                <p>
                    I don’t blame you.
                </p>
                <p>
                    One article about the dangers of alcohol and you decide never to drink
                    alcohol again. One missed workout and you think you’re going to die of
                    heart disease at forty. “If he said that the way he did, I’m sure that
                    means we’re going to have a fight”. Better prepare.
                </p>
                <p>
                    No evidence. No reason to believe one is in danger. And yet, we’re all too
                    ready to get our swords out just in case.
                </p>
                <p>
                    Suffering from an anxiety disorder means that you pay very close attention
                    to possible threats in your environment*.
                </p>
                <p>
                    The future becomes one big shitstorm of possible negative outcomes. A
                    lifeboat of sorts. Except pretty soon, you find a way to use the lifeboat
                    even though you’re surrounded by sand.
                </p>
                <p>
                    It’s like saying “ouch!” before the needle pricks your finger. You focus on
                    the needle (an external object), in the belief that you will be able to
                    control the pain it’s about to subject you to.
                </p>
                <p>
                    But the needle isn’t a source of pain, it triggers pain inside you. The
                    words someone says to you aren’t a source of pain, they trigger pain inside
                    you.
                </p>
                <p>
                    “Things themselves touch not the soul, not in the least degree, nor have
                    they admission to the soul. The soul turns and moves itself alone, and
                    whatever judgements it may think proper to make, such it makes for itself
                    the things which present themselves to it”.
                </p>
                <p>
                    Marcus Aurelius, Meditations
                </p>
                <p>
                    In focusing on this external object, the needle, which you have no control
                    over, you deny the one thing you do have control over: you.
                </p>
                <p>
                    However, having control means taking control.
                </p>
                <p>
                    In the beginning, that might be too hard to handle.
                </p>

                <h2>When freedom means responsibility</h2>
                            
                <p>
                    An anxious mind is a trickster, turning worry into a coping mechanism that
                    gives you a false sense of authority over your life. Without it, you feel
                    vulnerable.
                </p>
                <p>
                    But it isn’t control, is it? It’s avoidance.
                </p>
                <p>
                    You worry about whether you should dip your toe in the ocean instead of
                    just dipping your toe in the ocean. You worry about whether you should
                    apply for that job instead of just applying.
                </p>
                <p>
                    You worry about how you should live your best life instead of just living.
                </p>
                <p>
                    In the process, you think you might avoid pain. But you and I both know
                    that freedom doesn’t come without it.
                </p>
                <p>
                    Freedom isn’t a ride on the coast, in the middle of July, where you can
                    drink as much as you want without suffering the consequences.
                </p>
                <p>
                    Freedom is independence and independence is self-reliance.
                </p>
                <p>
                    Yeah, can’t rely on alcohol, sex, money, status or someone else’s love.
                    It’s just you.
                </p>
                <p>
                    So when you avoid pain, climbing inside a corner of your room like a
                    spider, you’re really just avoiding your feelings. And you can’t run from
                    yourself, can you?
                </p>
                <p>
                    Because in the end, no matter how far you go and no matter what you’re
                    running from, it will still be there. And like a land mine left behind
                    after the Second World War waiting to be stepped on, you’re just waiting
                    for someone to flick that switch.
                </p>
                <h2>When you decide to move on</h2>
                <p>
                    Sometimes, the best thing you can do for yourself is let go of your
                    identity.
                </p>
                <p>
                    “I’m an anxious person”, do you still need that? Is it helping? “I’m an
                    angry person”, is that still working for you? “I’m an asshole”, sure you
                    are. Do you want to die one too?
                </p>
                <p>
                    From someone telling us at school that we’re this or that type of kid to
                    lovers putting labels on our foreheads and saying: “Oh yeah, that’s what
                    you are”, we’ve all been given some sort of identity.
                </p>
                <p>
                    The beauty of it is, however powerful constructions might be, they only
                    exist to the extent to which we want them to.
                </p>
                <p>
                    Ultimately, the change begins here, within the vicinity of our own
                    consciousness.
                </p>
                <p>
                    The roles we play within society, the different identities we have amongst
                    our work colleagues, our family, our friends, or in the supermarket, are
                    useful and carry a lot of weight in our lives.
                </p>
                <p>
                    But if anything, life is forever changing, and if we don’t evolve with it,
                    we will be left behind with tools that out-date our needs.
                </p>
                <p>
                    The baby’s favourite blanket will eventually be abandoned or passed on to
                    another baby. The little boy’s favourite teddy bear will be snuggled up in
                    his arms until he grows up and no longer needs it.
                </p>
                <p>
                    Would it look odd if your boyfriend slept with his childhood teddy bear?
                    Yeah, you may start hearing some alarm bells ringing. Instead, the
                    boyfriend takes on the role of a boyfriend. Maybe later he’ll take on the
                    role of a husband, and father and so on.
                </p>
                <p>
                    An anxiety disorder emerges at different times in our lives and for
                    different reasons.
                </p>
                <p>
                    But that doesn’t mean that it was meant to stay with us forever.
                </p>
                <p>
                    And is it worth recognizing that one is capable of finding solace within
                    oneself?
                </p>
                <p>
                    Well, you won’t find it anywhere else.
                </p>
                <p class="p-small">
                    *Temperament and Anxiety Disorders, Koraly Pérez-Edgar, PhD*, Nathan A.
                    Fox, PhD University of Maryland, 3304 Benjamin Building, College Park, MD
                    20742, US.
                </p>
            </article>
        </main>
    </div>
  );
}

export default anxiety;
