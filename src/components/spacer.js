import React from "react"

export default function Spacer(props) {
  return (
    <section className="spacer">
        <span></span>
        <span></span>
        <span></span>
    </section>
  )
}