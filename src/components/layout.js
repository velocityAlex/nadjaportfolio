import React from "react"
import { Link } from "gatsby"
import Menu from './menu'


export default function Layout({ children }) {
  return (

    <header>
        <h1>
            <Link to="/">
            Nadja Moore Portfolio
            </Link>
        </h1>

        <Menu />
        
    </header>

  )
}