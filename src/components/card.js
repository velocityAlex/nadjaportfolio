import React from "react"
import { Link } from "gatsby"

export default function Card(props) {
  return (
    <div className="card">
        <h3>{props.articleHeader}</h3>
        <p class="p-large">{props.articleExcerpt}</p>
        <Link className="stretched-link" to={props.link}>
            <p style={{ display: `inline` }}>Go to Article</p>
        </Link>
    </div>
  )
}