import React, {useState} from 'react';
import { Link } from "gatsby"
import styled from 'styled-components';

const MenuIcon = styled.button`
    position: ${({nav}) => (nav ? "fixed" : "absolute" )};
    top: 85px;
    right: 9rem;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    width: 1.5rem;
    height: 1.5rem;
    background:transparent;
    border: none;
    cursor: pointer;
    z-index: 6;
    
    @media (max-width: 1000px) {
        right: 3rem;
    }

    @media (max-width: 768px) {
        right: 3rem;
        top: 70px;
    }

    &:focus{
        outline:none;
    }

    div{
        width: 1.5rem;
        height:0.2rem;
        background: black;
        border-radius: 5px;
        transform-origin:1px;
        position:relative;
        transition: opacity 400ms, transform 500ms;
        
        :first-child{
            transform: ${({nav}) => (nav ? "rotate(45deg)" : "rotate(0)" )}
        }
        :nth-child(2){
            opacity: ${({nav}) => (nav ? "0" : "1" )}
        }
        :nth-child(3){
            transform: ${({nav}) => (nav ? "rotate(-45deg)" : "rotate(0)") }
        }
    }
`

const MenuLinks = styled.div`
    display:flex;
    flex-direction:column;
    justify-content:center;
    align-content:center;
    text-align:center;
    height:100vh;
    width:100%;
    background: #e4e4e4;
    position: fixed;
    z-index: 4;
    top:0;
    right:0;
    transition: transform 500ms ease-out;
    transform: ${({nav}) => (nav ? "translateX(0)" : "translateX(100%)") };

    ul{
        list-style-type:none;
        padding:0;
    }

    li{
        margin-top: 4rem;
        font-size: 2rem;
        
    }

    a{
        text-decoration: none;
        color: black;


        :hover{
            color:#342983;
            letter-spacing: 6px;
            
        }
    }
`

function Menu() {
  
    const [nav, showNav] = useState(false)
    
    return (
    <div>
        <MenuIcon nav={nav} onClick={() => showNav(!nav)}>
            <div />
            <div />
            <div />
        </MenuIcon>
        <MenuLinks nav={nav}>
            <ul>
                <li>
                    <Link to="/">HOME</Link>
                </li>
                <li>
                    <Link to="/guide-to-seo">A BEGINNER’S GUIDE TO SEO</Link>
                </li>
                <li>
                    <Link to="/easy-health-bar">EASY</Link>
                </li>
                <li>
                    <Link to="/content-writing">WHY CONTENT WRITING MATTERS</Link>
                </li>
                <li>
                    <Link to="/anxiety">STOP ANXIETY FROM RUINING YOUR LIFE</Link>
                </li>
                <li>
                    <Link to="/short-hair">IF YOU CUT YOUR HAIR, YOU’LL LOOK LIKE A BOY</Link>
                </li>
            </ul>
        </MenuLinks>
    </div>
  );
}

export default Menu;