module.exports = {
  siteMetadata: {
    title: 'Nadja Moore Copywriter',
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Nadja Moore Copywriter',
        short_name: 'Nadja Moore Copywriter',
        start_url: '/',
        icon: 'src/images/gatsby-icon.png',
      },
    },
  ],
};
